select * from visitor;

select * from adult;

select * from visitors;

select v.id, v.visitor_id, v.staff_id, v.adult_id, v.emergency_contact_id, v.checkin, v.checkout from visitors v inner join(select visitor_id, max(checkin) as max_checkin from visitors where checkout is null group by visitor_id) v2 on v.visitor_id = v2.visitor_id and v.checkin = v2.max_checkin and v.visitor_id = 130;

select v.id, v.first_name, v.last_name, v.date_of_birth, vt.id as v_id, vt.visitor_type from visitor v, visitor_types vt where v.first_name Like '%' and v.last_name LIKE '%' and vt.id = v.visitor_type_id;

update visitors set checkout=now() where visitor_id < 130;

select * from visitors where checkout is null;

select * from visitor where id = 130;

select * from staff;

select * from emergency_contact;

select * from visitor;

select * from visitor_types;

select * from adult;

select * from visitors;

select v3.id as visitors_id, visitor1.id, visitor1.first_name, visitor1.surname, visitor1.age_id, ages.age, visitor1.gender_id, genders.gender as gender, v4.id as v_id, v4.visitor_type 
  from visitor visitor1
 inner join(select v.id, v.visitor_id, v.emergency_contact_id, v.checkin, v.checkout 
		      from visitors v 
			inner join (select visitor_id, max(checkin) as max_checkin, checkout 
				          from visitors 
				         where checkout is null group by visitor_id) v2 
				            on v.visitor_id = v2.visitor_id and v.checkin = v2.max_checkin) v3 
    on visitor1.id = v3.visitor_id
  inner join(select id, visitor_type 
              from visitor_types) v4 
     on visitor1.visitor_type_id = v4.id
  inner join(select id, gender
               from genders) genders
     on visitor1.gender_id = genders.id
  inner join (select id, age
                from ages) ages
     on visitor1.age_id = ages.id
    and visitor1.first_name Like '%' 
    and visitor1.surname LIKE '%';
    
    
    



select *, timediff(checkout, checkin) as duration from visitors where checkout is not null order by visitor_id;

select *, VISITOR_AGE(date_of_birth) from visitor;



SELECT YEAR(NOW()) - YEAR(date_of_birth) from visitor;

SELECT DAY(NOW());


select s.id, s.first_name, s.last_name, s.mobile_number, s.clinic_number, c.id as clinic_id, c.clinic from staff s, clinics c where s.first_name = 'August' and s.last_name = 'July' and s.mobile_number = '0435111111' and c.id = s.clinic_id;

select * from visitor where id=12;

create event system_checkout
	on schedule
      every 1 day
      starts '2018-07-06 22:59:00' on completion preserve enable
	do update visitors set checkout = now() where checkout is null;

show processlist;

show events;

select * from visitors;

select * from visitor;

select * from visitor where last_name = 'D''Cruz';

create event system_checkout
	on schedule
      every 1 minute
      starts current_timestamp() + interval 1 hour
	on completion preserve
	do update visitors set checkout = now() where checkout is null;
    
create event system_checkout
	on schedule
      every 1 day
      starts str_to_date( date_format(now(), '%Y%m%d 2359'), '%Y%m%d %H%i' )
	on completion preserve
do update visitors set checkout = now() where checkout is null;
    
drop event system_checkout;

select * from adult;

use funonfourd;

select id, visitor_age(date_of_birth) as age from visitor;

select * from visitor;

select vis.id, vis.first_name, vis.last_name, vis.age, v4.id as v_id, v4.visitor_type from visitor vis inner join(select v.id, v.visitor_id, v.staff_id, v.adult_id, v.emergency_contact_id, v.checkin, v.checkout from visitors v inner join (select visitor_id, max(checkin) as max_checkin, checkout from visitors where checkout is null group by visitor_id) v2 on v.visitor_id = v2.visitor_id and v.checkin = v2.max_checkin) v3 on vis.id = v3.visitor_id inner join(select id, visitor_type from visitor_types) v4 on vis.visitor_type_id = v4.id and vis.first_name Like 'Josh%' and vis.last_name LIKE 'Mouse%';

select * from visitor_types;

delete from visitor where id = 2;

select * from visitors;


select * from visitors v 
 inner join (select visitor_id, max(checkin) as max_checkin, checkout 
               from visitors 
			  where checkout is null group by visitor_id) v2 
				 on v.visitor_id = v2.visitor_id and v.checkin = v2.max_checkin and v.visitor_id = 160;
                       
                       
call get_visitor_group_id(160, @visitor_group_id);

select @visitor_group_id;

call get_visitor_group_id(160);

select vis.id, vis.first_name, vis.last_name, vis.age, v4.id as v_id, v4.visitor_type 
  from visitor vis 
 inner join(select v.id, v.visitor_id, v.staff_id, v.adult_id, v.emergency_contact_id, v.checkin, v.checkout 
             from visitors v
            where v.visitor_group_id = 2) v2
    on vis.id = v2.visitor_id 
 inner join(select id, visitor_type from visitor_types) v4 
    on vis.visitor_type_id = v4.id and vis.id != 162;


select * from visitors;

select * 
  from visitors v 
 inner join (select id, max(checkin) as max_checkin
              from visitors 
			 where checkout is null
             group by id) v1
 where v.id = v1.id;

select id, max(checkin) as max_checkin
              from visitors 
			 where checkout is null
             group by first_name, surname;
             
drop table visitor;

drop table visitors;

drop table genders;

drop table age;

drop table visitor_type;

drop table ratings;

select * from visitor;

select * from emergency_contacts;

select * from visitors;

call visitors_checked_in('jan', '');

drop procedure visitors_checked_in;

select v.id, v.visitor_id, v.emergency_contact_id, v.checkin, v.checkout 
  from visitors v 
 inner join (select visitor_id, max(checkin) as max_checkin, checkout 
			   from visitors 
			  where checkout is null group by visitor_id) v2 
				 on v.visitor_id = v2.visitor_id and v.checkin = v2.max_checkin;

select visitor_id, max(checkin) as max_checkin, checkout 
			   from visitors 
			  where checkout is null group by visitor_id;
              
select * from visitor;

select * from emergency_contacts;

select * from visitor where first_name like 'jan%';

select * from ratings;

select * from visitor;


select v3.id as visitors_id, visitor1.id, visitor1.first_name, visitor1.surname, visitor1.age_id, ages.age, visitor1.gender_id, genders.gender as gender, v4.id as visitor_type_id, v4.visitor_type 
      from visitor visitor1
     inner join(select v.id, v.visitor_id, v.emergency_contact_id, v.checkin, v.checkout 
		          from visitors v 
				inner join (select visitor_id, max(checkin) as max_checkin, checkout 
				              from visitors 
				             where checkout is null group by visitor_id) v2 
				                on v.visitor_id = v2.visitor_id and v.checkin = v2.max_checkin) v3 
        on visitor1.id = v3.visitor_id
     inner join(select id, visitor_type 
              from visitor_types) v4 
        on visitor1.visitor_type_id = v4.id
     inner join(select id, gender
               from genders) genders
        on visitor1.gender_id = genders.id
	inner join (select id, age
                from ages) ages
        on visitor1.age_id = ages.id
       and lower(visitor1.first_name) Like CONCAT(lower(''), '%')
       and lower(visitor1.surname) LIKE CONCAT(lower(''), '%');
       
select v3.first_name, v3.surname, v3.age, v3.gender, v3.visitor_type,  v4.rating, e1.contact_name, e1.contact_number, e1.is_staff, v1.checkin, v1.checkout
  from visitors v1
 inner join (select id, contact_name, contact_number, is_staff
               from emergency_contacts) e1
		 on v1.emergency_contact_id = e1.id
 inner join (select id, first_name, surname, age_id, a1.age, gender_id, g1.gender, visitor_type_id, vt1.visitor_type
               from visitor v2
               inner join (select id as a_id, age
                             from ages) a1
					   on v2.age_id = a1.a_id
			   inner join (select id as g_id, gender
                             from genders) g1
					   on v2.gender_id = g1.g_id
			   inner join (select id as vt_id, visitor_type
                             from visitor_types) vt1
					   on v2.visitor_type_id = vt1.vt_id) v3
	on v1.visitor_id = v3.id
  left join (select id as r_id, rating
                             from ratings) v4
					   on v1.rating_id = v4.r_id
    where v1.checkout is not null
    and checkin between curdate() and now()
    order by surname;
    
call visitors_checked_out('','', curdate(), now());

call visitors_checked_in('','', curdate(), now());

select * from visitors where checkin > curdate() and checkout <= now();

select curdate();

drop procedure visitors_checked_out;

drop procedure visitors_checked_in;

drop index ix_visitor on visitor;

alter table visitor add UNIQUE KEY `ix_visitor` (`first_name`, `surname`, `age_id`, `gender_id`, `visitor_type_id`);

select * from visitor;

select v.id, v.first_name, v.surname, v.age_id, v.gender_id, vt.id as v_id, vt.visitor_type, a.id as a_id, a.age, g.id as g_id, g.gender from visitor v, visitor_types vt, ages a, genders g where v.first_name = "Sally" and v.surname = "Rabbit" and v.age_id = 3 and v.gender_id = 3 and vt.id = 1  and a.id = v.age_id and g.id = v.gender_id and vt.id = v.visitor_type_id;


select v.id, v.first_name, v.surname, v.age_id, v.gender_id, vt.id as v_id, vt.visitor_type, a.id as a_id, a.age, g.id as g_id, g.gender  from visitor v, visitor_types vt, ages a, genders g where v.first_name = "Sally" and v.surname = "Rabbit" and v.age_id = 3 and v.gender_id = 3 and vt.id = 1   and a.id = v.age_id and g.id = v.gender_id and vt.id = v.visitor_type_id;


select id, first_name, surname, age_id, a1.age, gender_id, g1.gender, v2.rating_id, r1.rating, visitor_type_id, vt1.visitor_type
               from visitor v2
               inner join (select id as a_id, age
                             from ages) a1
					   on v2.age_id = a1.a_id
			   inner join (select id as g_id, gender
                             from genders) g1
					   on v2.gender_id = g1.g_id
			   inner join (select id as vt_id, visitor_type
                             from visitor_types) vt1
					   on v2.visitor_type_id = vt1.vt_id
			    left join (select id as r_id, rating
                             from ratings) r1
					   on v2.rating_id = r1.r_id;
                       
call visitors(curdate()-5, now());

drop procedure visitors;

select * from visitors;