use `funonfourd`;

create database funonfour character set utf8mb4 collate utf8mb4_unicode_ci;

create user `kiosk`@`%` identified by 'UCV#FL%ov@]1z,/KFp`G';
grant all on funonfour.* to `kiosk`@`%`;

create user `reports`@`%` identified by 'Rep0rts!';
grant select on funonfour.* to `reports`@`%`;
grant execute on funonfour.* to `reports`@`%`;

drop table if exists visitors;
drop table if exists visitor_group;
drop table if exists staff;
drop table if exists adult;
drop table if exists emergency_contact;
drop table if exists visitor;
drop table if exists visitor_types;
drop table if exists clinics;
drop table if exists adult_types;
DROP FUNCTION IF EXISTS VISITOR_AGE;

create table `visitor_types` (
	`id` MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT,
    `visitor_type` VARCHAR(100) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `ix_visitor_type` (`visitor_type`)
)ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATe=utf8mb4_unicode_ci; 
    
create table `genders` (
	`id` MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT,
    `gender` VARCHAR(10) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `ix_gender` (`gender`)
)ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATe=utf8mb4_unicode_ci; 

create table `ages` (
	`id` MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT,
    `age` int NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `ix_age` (`age`)
)ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

create table `ratings` (
	`id` MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT,
    `rating` varchar(20) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `ix_rating` (`rating`)
)ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

create table `visitor` (
    `id` MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT,
    `first_name` varchar(100) NOT NULL,
    `surname` varchar(100) NOT NULL,
    `age_id` MEDIUMINT(8) UNSIGNED NOT NULL,
    `gender_id` MEDIUMINT(8) UNSIGNED NOT NULL,
    `visitor_type_id` MEDIUMINT(8) UNSIGNED NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `ix_visitor` (`first_name`, `surname`, `age_id`, `gender_id`, `visitor_type_id`),
    FOREIGN KEY (`age_id`)
		REFERENCES ages(`id`)
        ON DELETE CASCADE,
	FOREIGN KEY (`gender_id`)
		REFERENCES genders(`id`)
        ON DELETE CASCADE,
	FOREIGN KEY (`visitor_type_id`)
		REFERENCES visitor_types(`id`)
        ON DELETE CASCADE
)ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

create table `emergency_contacts` (
    `id` MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT,
	`contact_name` varchar(200) NOT NULL,
    `contact_number` varchar(10) NOT NULL,
    `is_staff` bool default false,
    PRIMARY KEY (`id`),
    UNIQUE KEY `ix_emergency_contacts` (`contact_name`, `contact_number`, `is_staff`)
)ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

create table `visitors` (
    `id` MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT,
    `visitor_id` MEDIUMINT(8) UNSIGNED NOT NULL,
	`emergency_contact_id`  MEDIUMINT(8) UNSIGNED NOT NULL, 
    `rating_id` MEDIUMINT(8) UNSIGNED,
    `checkin` TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    `checkout` timestamp,
    PRIMARY KEY (`id`),
    FOREIGN KEY (`visitor_id`)
		REFERENCES visitor(`id`)
        ON DELETE CASCADE,
	FOREIGN KEY (`rating_id`)
		REFERENCES ratings(`id`)
        ON DELETE CASCADE
)ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

create event system_checkout
	on schedule
      every 1 day
      starts str_to_date( date_format(now(), '%Y%m%d 2359'), '%Y%m%d %H%i' )
	on completion preserve
do update visitors set checkout = now() where checkout is null;
