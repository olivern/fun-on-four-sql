DELIMITER //
create procedure visitors(v_date_from DATETIME, v_date_to DATETIME)
begin
	select v3.first_name, v3.surname, v3.age, v3.gender, v3.visitor_type,  v4.rating, e1.contact_name, e1.contact_number, e1.is_staff, v1.checkin, v1.checkout
	  from visitors v1
     inner join (select id, contact_name, contact_number, is_staff
                   from emergency_contacts) e1
		on v1.emergency_contact_id = e1.id
     inner join (select id, first_name, surname, age_id, a1.age, gender_id, g1.gender, visitor_type_id, vt1.visitor_type
                   from visitor v2
				  inner join (select id as a_id, age
                                from ages) a1
					 on v2.age_id = a1.a_id
			      inner join (select id as g_id, gender
                             from genders) g1
					 on v2.gender_id = g1.g_id
			      inner join (select id as vt_id, visitor_type
                                from visitor_types) vt1
					 on v2.visitor_type_id = vt1.vt_id) v3
	    on v1.visitor_id = v3.id
      left join (select id as r_id, rating
				   from ratings) v4
		on v1.rating_id = v4.r_id
     where checkin between v_date_from and v_date_to
     order by checkout;
end;