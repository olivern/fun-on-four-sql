use `funonfourd`;

create database funonfour character set utf8mb4 collate utf8mb4_unicode_ci;

create user `kiosk`@`%` identified by 'UCV#FL%ov@]1z,/KFp`G';

grant all on funonfour.* to `kiosk`@`%`;

drop table if exists visitors;
drop table if exists visitor_group;
drop table if exists staff;
drop table if exists adult;
drop table if exists emergency_contact;
drop table if exists visitor;
drop table if exists visitor_types;
drop table if exists clinics;
drop table if exists adult_types;

create table `visitor_types` (
	`id` MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT,
    `visitor_type` VARCHAR(100) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `ix_visitor_type` (`visitor_type`)
)ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATe=utf8mb4_unicode_ci; 
    
create table `adult_types` (
	`id` MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT,
    `adult_type` VARCHAR(100) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `ix_adult_type` (`adult_type`)
)ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATe=utf8mb4_unicode_ci; 

create table `clinics` (
	`id` MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT,
    `clinic` varchar(100) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `ix_clinic` (`clinic`)
)ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

create table `visitor` (
	`id` MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT,
    `first_name` varchar(100) NOT NULL,
    `last_name` varchar(100) NOT NULL,
    `age` int not null,
    `visitor_type_id` MEDIUMINT(8) UNSIGNED NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `ix_visitor` (`first_name`, `last_name`, `age`),
    FOREIGN KEY (`visitor_type_id`)
		REFERENCES visitor_types(`id`)
        ON DELETE CASCADE
)ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

create table `staff` (
	`id` MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT,
    `first_name` varchar(100) NOT NULL,
    `last_name` varchar(100) NOT NULL,
    `mobile_number` varchar(10) not null,
    `clinic_id` MEDIUMINT(8) UNSIGNED NOT NULL,
    `clinic_number` varchar(10) not null,
    PRIMARY KEY (`id`),
    UNIQUE KEY `ix_visitor` (`first_name`, `last_name`, `mobile_number`),
    FOREIGN KEY (`clinic_id`)
		REFERENCES clinics(`id`)
        ON DELETE CASCADE
)ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

create table `adult` (
	`id` MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT,
    `first_name` varchar(100) NOT NULL,
    `last_name` varchar(100) NOT NULL,
    `mobile_number` varchar(10) not null,
    `email` varchar(256) NOT NULL,
    `adult_type_id` MEDIUMINT(8) UNSIGNED NOT NULL,
    `receive_updates` boolean NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `ix_adult` (`first_name`, `last_name`, `mobile_number`),
    FOREIGN KEY (`adult_type_id`)
		REFERENCES adult_types(`id`)
        ON DELETE CASCADE
)ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

create table `emergency_contact` (
	`id` MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT,
    `contact_name` varchar(100) NOT NULL,
    `contact_number` varchar(10) not null,
    `contact_relationship` varchar(100) not null,
    PRIMARY KEY (`id`)
)ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

create table `visitor_group` (
	`id` MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT,
	`checked_at` TIMESTAMP NOT NULL,
    PRIMARY KEY (`id`)
)ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

create table `visitors` (
	`id` MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT,
    `visitor_id` MEDIUMINT(8) UNSIGNED NOT NULL,
    `adult_id` MEDIUMINT(8) UNSIGNED,
    `staff_id` MEDIUMINT(8) UNSIGNED,
    `emergency_contact_id` MEDIUMINT(8) UNSIGNED,
    `visitor_group_id` MEDIUMINT(8) UNSIGNED,
    `checkin` TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    `checkout` timestamp,
    PRIMARY KEY (`id`),
    FOREIGN KEY (`adult_id`)
		REFERENCES adult(`id`)
        ON DELETE CASCADE,
	FOREIGN KEY (`staff_id`)
		REFERENCES staff(`id`)
        ON DELETE CASCADE,
	FOREIGN KEY (`visitor_id`)
		REFERENCES visitor(`id`)
        ON DELETE CASCADE,
	FOREIGN KEY (`emergency_contact_id`)
		REFERENCES emergency_contact(`id`)
        ON DELETE CASCADE,
	FOREIGN KEY (`visitor_group_id`)
        REFERENCES visitor_group(`id`)
)ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

create event system_checkout
	on schedule
      every 1 day
      starts str_to_date( date_format(now(), '%Y%m%d 2359'), '%Y%m%d %H%i' )
	on completion preserve
do update visitors set checkout = now() where checkout is null;