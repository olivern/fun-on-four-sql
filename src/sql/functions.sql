use funonfourd;

DELIMITER //

DROP FUNCTION IF EXISTS VISITOR_AGE;
CREATE FUNCTION VISITOR_AGE(p_date_of_birth timestamp) RETURNS INT
deterministic
BEGIN
	DECLARE age int;
    DECLARE now_date timestamp;
    DECLARE now_day int;
    DECLARE birth_day int;
    
    SELECT YEAR(NOW()) - YEAR(p_date_of_birth) INTO age;
    
    SELECT DAY(NOW()) INTO now_day;
    
    SELECT DAY(p_date_of_birth) INTO birth_day;
    
    IF now_day < birth_day THEN
		SET age = age - 1;
    END IF;
    
    RETURN age;
END