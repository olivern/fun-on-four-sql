

use funonfour;

drop procedure patch_visitor_age;

DELIMITER //

create procedure patch_visitor_age()
begin
	declare v_done int default false;
    declare v_age INT;
    declare v_id mediumint(8);
    
    declare cur1 cursor for 
     select id, visitor_age(date_of_birth) as age 
       from visitor
		for update;
        
    declare CONTINUE HANDLER FOR NOT FOUND SET v_done = TRUE;
    
    open cur1;
    
    read_loop: loop
			fetch cur1 into v_id, v_age;
            if v_done then
				leave read_loop;
			end if;
            if v_age = -1 then
				set v_age = 0;
			end if;
            update visitor set age = v_age where id = v_id;
    end loop;
    
    close cur1;
end;

drop procedure get_visitor_group_id;

DELIMITER //
create procedure get_visitor_group_id(v_visitor_id MEDIUMINT(8))
begin
	declare v_visitor_group_id MEDIUMINT(8);
    
    declare continue handler for not found set v_visitor_group_id = null;
    
    select visitor_group_id
      into v_visitor_group_id
      from visitors v 
	 inner join (select visitor_id, max(checkin) as max_checkin, checkout 
                   from visitors 
			      where checkout is null group by visitor_id) v2 
				     on v.visitor_id = v2.visitor_id and v.checkin = v2.max_checkin and v.visitor_id = v_visitor_id;
                     
	select v_visitor_group_id;
end;