
use funonfour;

alter table visitor add column age int after last_name;

call patch_visitor_age();

alter table visitor modify age int not null;

alter table visitor drop index ix_visitor, add UNIQUE KEY `ix_visitor` (`first_name`, `last_name`, `age`);

alter table visitor drop column date_of_birth;

alter table visitors add column `visitor_group_id` MEDIUMINT(8) UNSIGNED after emergency_contact_id;

create table `visitor_group` (
	`id` MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT,
	`created_at` TIMESTAMP NOT NULL,
    PRIMARY KEY (`id`)
)ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

alter table visitors add FOREIGN KEY (`visitor_group_id`) REFERENCES visitor_group(`id`);

