DELIMITER //
create procedure visitors_checked_out(v_first_name VARCHAR(200), v_surname VARCHAR(200), v_date_from DATETIME, v_date_to DATETIME)
begin
	select v3.id as visitors_id, visitor1.id, visitor1.first_name, visitor1.surname, visitor1.age_id, ages.age, visitor1.gender_id, genders.gender as gender, v4.id as visitor_type_id, v4.visitor_type 
      from visitor visitor1
     inner join(select v.id, v.visitor_id, v.emergency_contact_id, v.checkin, v.checkout 
		          from visitors v 
				inner join (select visitor_id, max(checkin) as max_checkin, checkout 
				              from visitors 
				             where checkout is not null
							   and checkin between v_date_from and v_date_to
							 group by visitor_id) v2 
				                on v.visitor_id = v2.visitor_id and v.checkin = v2.max_checkin) v3 
        on visitor1.id = v3.visitor_id
     inner join(select id, visitor_type 
              from visitor_types) v4 
        on visitor1.visitor_type_id = v4.id
     inner join(select id, gender
               from genders) genders
        on visitor1.gender_id = genders.id
	inner join (select id, age
                from ages) ages
        on visitor1.age_id = ages.id
       and lower(visitor1.first_name) Like CONCAT(lower(v_first_name), '%')
       and lower(visitor1.surname) LIKE CONCAT(lower(v_surname), '%');
end;