
insert into visitor_types (visitor_type) values('Inpatient');
insert into visitor_types (visitor_type) values('Outpatient');
insert into visitor_types (visitor_type) values('Sibling / Friend');

insert into genders (gender) values('');
insert into genders (gender) values('Male');
insert into genders (gender) values('Female');


insert into ages (age) values(0);
insert into ages (age) values(1);
insert into ages (age) values(2);
insert into ages (age) values(3);
insert into ages (age) values(4);
insert into ages (age) values(5);
insert into ages (age) values(6);
insert into ages (age) values(7);
insert into ages (age) values(8);
insert into ages (age) values(9);
insert into ages (age) values(10);
insert into ages (age) values(11);
insert into ages (age) values(12);
insert into ages (age) values(13);
insert into ages (age) values(14);
insert into ages (age) values(15);
insert into ages (age) values(16);
insert into ages (age) values(17);
insert into ages (age) values(18);

insert into ratings (rating) values('Positive');
insert into ratings (rating) values('Negative');
insert into ratings (rating) values('Neutral');